package fr.uge.client.gui;

import java.awt.Color;
import java.awt.Font;
import java.awt.GridBagConstraints;
import java.awt.GridBagLayout;
import java.awt.Insets;

import javax.swing.BorderFactory;
import javax.swing.JButton;
import javax.swing.JPanel;
import javax.swing.JScrollBar;
import javax.swing.JScrollPane;
import javax.swing.JTextField;
import javax.swing.JTextPane;
import javax.swing.ScrollPaneConstants;
import javax.swing.border.TitledBorder;

/**
 * @author Kevin Martin
 * @version 1.2
 *
 * DO NOT MODIFY UNDER PENALTY OF HAVING GRAPHICAL INTERFACE PROBLEMS !
 * ALL CHANGES AND PROBLEMS OR BUGS THAT MAY FOLLOWS ARE UNDER YOUR RESPONSABILITY !
 */

public class AppPanel extends JPanel {

    private final JTextPane output;
    private final JTextField message;
    private final JButton sendMessage;

    public AppPanel(){
        this.setLayout(new GridBagLayout());
        this.setBackground(Color.BLACK);
        this.setEnabled(true);
        this.setForeground(Color.BLACK);
        this.setBorder(BorderFactory.createTitledBorder(BorderFactory.createLineBorder(Color.WHITE), null));

        JScrollPane scrollMessage = genrerateScrollPane();
        output = generateMessagePane();

        scrollMessage.setViewportView(output);
        this.add(scrollMessage, setupConstraints(0 , 0, 1.0, 10.0, GridBagConstraints.BOTH, GridBagConstraints.CENTER, new Insets(0, 5, 0, 5)));


        JPanel panel1 = new JPanel();
        panel1.setLayout(new GridBagLayout());
        panel1.setBackground(Color.BLACK);
        panel1.setForeground(Color.BLACK);
        this.add(panel1, setupConstraints(0 , 1, 1.0, 0.05, GridBagConstraints.BOTH, GridBagConstraints.CENTER, new Insets(0, 0, 0, 0)));

        message = new JTextField();
        Font messageFont = this.getFont("Consolas", Font.PLAIN, 12, message.getFont());
        if (messageFont != null) message.setFont(messageFont);
        panel1.add(message, setupConstraints(0 , 0, 1.0, 1.0, GridBagConstraints.HORIZONTAL, GridBagConstraints.WEST, new Insets(0, 5, 0, 5)));

        sendMessage = new JButton("Envoyer");
        panel1.add(sendMessage, setupConstraints(1 , 0, 0.1, 1.0, GridBagConstraints.HORIZONTAL, GridBagConstraints.CENTER, new Insets(5, 5, 5, 5)));

    }

    public JTextPane getOutput(){
        return output;
    }

    public JTextField getMessage(){
        return message;
    }

    public JButton getSendMessage(){
        return sendMessage;
    }

    private GridBagConstraints setupConstraints(int gridx, int gridy, double weightx, double weighty, int fill, int anchor, Insets insets){
        GridBagConstraints gbc = new GridBagConstraints();
        gbc.gridx = gridx; gbc.gridy = gridy;
        gbc.weightx = weightx; gbc.weighty = weighty;
        gbc.anchor = anchor; gbc.fill = fill;
        gbc.insets = insets;
        return gbc;
    }

    private JScrollPane genrerateScrollPane(){
        JScrollPane scroll = new JScrollPane();
        JScrollBar scrollBar = new JScrollBar(JScrollBar.VERTICAL) {
            @Override
            public boolean isVisible() {
                return true;
            }
        };
        scrollBar.putClientProperty("JScrollBar.fastWheelScrolling", Boolean.TRUE);
        scroll.setVerticalScrollBarPolicy(JScrollPane.VERTICAL_SCROLLBAR_NEVER);
        scroll.setVerticalScrollBar(scrollBar);
        scroll.setBackground(Color.BLACK);
        scroll.setEnabled(true);
        scroll.setForeground(Color.BLACK);
        scroll.setHorizontalScrollBarPolicy(ScrollPaneConstants.HORIZONTAL_SCROLLBAR_NEVER);
        scroll.setBorder(BorderFactory.createTitledBorder(BorderFactory.createLineBorder(Color.WHITE), "Chat", TitledBorder.CENTER, TitledBorder.DEFAULT_POSITION, null, Color.WHITE));
        return scroll;
    }

    private JTextPane generateMessagePane(){
        JTextPane text = new JTextPane();
        text.setBackground(Color.BLACK);
        text.setContentType("text/plain");
        text.setEditable(false);
        text.setFocusTraversalPolicyProvider(false);
        Font outputFont = this.getFont("Consolas", Font.PLAIN, 12, text.getFont());
        if (outputFont != null) text.setFont(outputFont);
        text.setForeground(Color.BLACK);
        text.setText("");
        return text;
    }

    private Font getFont(String fontName, int style, int size, Font currentFont) {
        if (currentFont == null)
            return null;
        String resultName;
        if (fontName == null) resultName = currentFont.getName();
        else {
            Font testFont = new Font(fontName, Font.PLAIN, 10);
            if (testFont.canDisplay('a') && testFont.canDisplay('1')) resultName = fontName;
            else resultName = currentFont.getName();
        }
        return new Font(resultName, style >= 0 ? style : currentFont.getStyle(), size >= 0 ? size : currentFont.getSize());
    }
}
