package fr.uge.client.gui;

import java.awt.Color;
import java.awt.Dimension;
import java.awt.event.ActionEvent;
import java.awt.event.KeyAdapter;
import java.awt.event.KeyEvent;
import java.nio.file.Path;

import javax.swing.JFrame;
import javax.swing.text.BadLocationException;
import javax.swing.text.Style;
import javax.swing.text.StyleConstants;
import javax.swing.text.StyledDocument;

import fr.uge.client.ClientChatHack;

/**
 * @author Kevin Martin
 * @version 1.2
 *
 * DO NOT MODIFY AppFrame() AND actionPerformed() UNDER PENALTY OF HAVING GRAPHICAL INTERFACE PROBLEMS !
 * YOU CAN ADD OR EDIT EVERY METHOD WHICH START BY display IF YOU NEED A NEW METHOD.
 * ALL CHANGES AND PROBLEMS OR BUGS THAT MAY FOLLOWS ARE UNDER YOUR RESPONSABILITY !
 */

public class AppFrame extends JFrame {

    private final AppPanel panel;
    private final Style msgStyle, publicStyle, privateStyle, greenStyle, redStyle, pendingStyle;

    // TODO change type if you need
    private final ClientChatHack client;

    // TODO change type if you need
    public AppFrame(String name, ClientChatHack client) {
        this.setTitle(name);
        this.setPreferredSize(new Dimension(800, 450));
        this.setDefaultCloseOperation(EXIT_ON_CLOSE);

        this.client = client;
        panel = new AppPanel();



        msgStyle = panel.getOutput().addStyle("style", null);
        StyleConstants.setForeground(msgStyle, Color.WHITE);
        publicStyle = panel.getOutput().addStyle("style", null);
        StyleConstants.setForeground(publicStyle, Color.CYAN);
        privateStyle = panel.getOutput().addStyle("style", null);
        StyleConstants.setForeground(privateStyle, Color.ORANGE);
        greenStyle = panel.getOutput().addStyle("style", null);
        StyleConstants.setForeground(greenStyle, Color.GREEN);
        redStyle = panel.getOutput().addStyle("style", null);
        StyleConstants.setForeground(redStyle, Color.RED);
        pendingStyle = panel.getOutput().addStyle("style", null);
        StyleConstants.setForeground(pendingStyle, Color.YELLOW);

        panel.getSendMessage().addActionListener(e -> {
            try {
                actionPerformed(e);
            } catch (BadLocationException | InterruptedException ex) {
                ex.printStackTrace();
            }
        });
        panel.getMessage().addKeyListener(new KeyAdapter() {
            @Override
            public void keyTyped(KeyEvent e) {
                if (e.getKeyChar() == '\n') {
                    try {
                        actionPerformed(null);
                    } catch (BadLocationException | InterruptedException ex) {
                        ex.printStackTrace();
                    }
                }
            }
        });
        this.setContentPane(panel);
        this.pack();
    }

    // Client Side
    public void actionPerformed(ActionEvent e) throws BadLocationException, InterruptedException {
        String msg = getPanel().getMessage().getText();
        if (!msg.equals(""))
            client.sendCommand(msg);
        getPanel().getMessage().setText("");
    }

    public AppPanel getPanel(){
        return panel;
    }

    // DISPLAY METHODS

    public void displayPendingConnection(String pseudo) throws BadLocationException {
        StyledDocument doc = panel.getOutput().getStyledDocument();
        doc.insertString(doc.getLength(), "Pending connection : "+ pseudo +"\n", pendingStyle);
    }

    public void displayAcceptedConnection(String pseudo) throws BadLocationException {
        StyledDocument doc = panel.getOutput().getStyledDocument();
        doc.insertString(doc.getLength(), "Accepted connection : "+ pseudo +"\n", greenStyle);
    }

    public void displayDeclinedConnection(String pseudo) throws BadLocationException {
        StyledDocument doc = panel.getOutput().getStyledDocument();
        doc.insertString(doc.getLength(), "Declined connection : "+ pseudo +"\n", redStyle);
    }

    /**
     * Display a message to alert other connected users of a new user's connection.
     * @param pseudo The username of the user who logged in
     * @throws BadLocationException Error
     */
    public void displayUserConnection(String pseudo) throws BadLocationException {
        StyledDocument doc = panel.getOutput().getStyledDocument();
        doc.insertString(doc.getLength(), pseudo+" connected.\n", greenStyle);
    }

    /**
     * Display a message to alert other connected users of a new user's disconnection.
     * @param pseudo The username of the user who has logged out
     * @throws BadLocationException Error
     */
    public void displayUserDisconnection(String pseudo) throws BadLocationException {
        StyledDocument doc = panel.getOutput().getStyledDocument();
        doc.insertString(doc.getLength(), pseudo+" disconnected.\n", redStyle);
    }

    /**
     * Displays a message indicating that a user has closed the private connection with him
     * @param pseudo The username of the user who closed the private connection with him
     * @throws BadLocationException Error
     */
    public void displayPrivateCloseConnection(String pseudo) throws BadLocationException {
        StyledDocument doc = panel.getOutput().getStyledDocument();
        doc.insertString(doc.getLength(), pseudo+" closed the connection.\n", redStyle);
    }

    /**
     * Displays a public message
     * @param pseudo The username of the user who sent a public message
     * @param message The message sent
     * @throws BadLocationException Error
     */
    public void displayPublicMessage(String pseudo, String message) throws BadLocationException {
        StyledDocument doc = panel.getOutput().getStyledDocument();
        doc.insertString(doc.getLength(), "["+pseudo+"] ", publicStyle);
        doc.insertString(doc.getLength(), ": "+message+"\n", msgStyle);
    }

    /**
     * Displays a private message
     * @param pseudo The username of the user who sent a private message
     * @param message The message sent
     * @param sending If sending = true => this user send a message at pseudo ELSE pseudo send him a message
     * @throws BadLocationException Error
     */
    public void displayPrivateMessage(String pseudo, String message, boolean sending) throws BadLocationException {
        StyledDocument doc = panel.getOutput().getStyledDocument();
        if (!sending)
            doc.insertString(doc.getLength(), "["+pseudo+" -> me] ", privateStyle);
        else
            doc.insertString(doc.getLength(), "[me -> "+pseudo+"] ", privateStyle);
        doc.insertString(doc.getLength(), ": "+message+"\n", msgStyle);
    }

	public void displayPrivateFile(String pseudo, String filename, boolean sending) throws BadLocationException {
		StyledDocument doc = panel.getOutput().getStyledDocument();
        if (sending)
            doc.insertString(doc.getLength(), filename+" sent\n", privateStyle);
        else
            doc.insertString(doc.getLength(), filename+" received\n", privateStyle);
	}

	public void displayRemovedConnection(String login) throws BadLocationException {
		StyledDocument doc = panel.getOutput().getStyledDocument();
        doc.insertString(doc.getLength(), "Closed connection :"+ login +"\n", redStyle);
    }

	public void displayUserNotExistConnection(String login) throws BadLocationException {
		StyledDocument doc = panel.getOutput().getStyledDocument();
        doc.insertString(doc.getLength(), "Client "+ login +" doesn't exists\n", redStyle);
	}
	
	public void displayFileNotExist(Path filename) throws BadLocationException {
		StyledDocument doc = panel.getOutput().getStyledDocument();
        doc.insertString(doc.getLength(), "File at "+ filename.toAbsolutePath().toString() +" doesn't exists\n", redStyle);
	}
}
