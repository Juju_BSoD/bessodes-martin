package fr.uge.client;

import java.io.IOException;

/**
 * interface made to facilitate the connect, read and write operations of the selectionKey registered by the selector of a client
 */
public interface Context {
	/**
	 * performs a read operation
	 * @throws IOException if there is a problem while reading
	 */
	void doRead() throws IOException;
	
	/**
	 * performs a write operation
	 * @throws IOException if there is a problem while writing
	 */
	void doWrite() throws IOException;
	
	/**
	 * performs a connect operation
	 * @throws IOException if there is a problem while connecting
	 */
	void doConnect() throws IOException;
}
