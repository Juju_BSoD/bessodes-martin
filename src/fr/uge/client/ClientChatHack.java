package fr.uge.client;

import java.io.File;
import java.io.IOException;
import java.io.UncheckedIOException;
import java.net.InetSocketAddress;
import java.nio.ByteBuffer;
import java.nio.channels.FileChannel;
import java.nio.channels.SelectionKey;
import java.nio.channels.Selector;
import java.nio.channels.ServerSocketChannel;
import java.nio.channels.SocketChannel;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.nio.file.StandardOpenOption;
import java.util.Arrays;
import java.util.Map.Entry;
import java.util.Optional;
import java.util.Random;
import java.util.concurrent.ArrayBlockingQueue;
import java.util.concurrent.ConcurrentHashMap;
import java.util.logging.Logger;

import javax.swing.text.BadLocationException;

import fr.uge.client.gui.AppFrame;
import fr.uge.reader.Message;

/**
 * A class representing the client who can connect to a server ChatHack
 */
public class ClientChatHack {

    private static Logger logger = Logger.getLogger(ClientChatHack.class.getName());

    private AppFrame app;
    private final InetSocketAddress serverAddress;
    private final ServerSocketChannel serverSocketChannel;
    private final SocketChannel sc;
    private final Selector selector;
    private String login, password;
    private final Path fileDir;
    private boolean auth;
    private PublicContext uniqueContext;
    private final ArrayBlockingQueue<String> commandQueue = new ArrayBlockingQueue<>(10);
    private ConcurrentHashMap<String, PrivateContext> pending = new ConcurrentHashMap<>(), accepted = new ConcurrentHashMap<>();
    private final Random random = new Random();
    
    //localhost port path pseudo (password)
    /**
     * initiate the client by setting the type of authentification, login and eventually his password and the path to locate the differents files. It also start the GUI.
     * @param args, arguments from the command line
     * @throws IOException if there is a problem while opening the selector + the SocketChannel of the client
     */
    public ClientChatHack(String[] args) throws IOException {
        login = args[3];
        fileDir = Paths.get(args[2]);
        String host = args[0], port = args[1], appName = login;
        if (args.length == 5) {
            password = args[4];
            auth = true;
            appName = "¤ "+login+" ¤";
        }
        serverSocketChannel = ServerSocketChannel.open().bind(new InetSocketAddress(host, 0));
        serverAddress = new InetSocketAddress(host, Integer.parseInt(port));
        sc = SocketChannel.open();
        selector = Selector.open();
        app = new AppFrame("ChatHack - "+appName,this);
    }
    
    private void healthCheckAccepted() {
    	while (!Thread.interrupted()) {
    		for (Entry<String, PrivateContext> entry : accepted.entrySet()) {
        		entry.getValue().queueMessage(ClientRequestUtils.HealthCheckRequest());
        	}
        	try {
    			Thread.sleep(1000);
    		} catch (InterruptedException e) {
    			e.printStackTrace();
    		}
    	}
    }

    /**
     * make the GUI appear
     */
    public void startApp(){
        app.setVisible(true);
    }
    
    
    /**
     * allow the interaction between the GUI and selector by sending commands
     * @param msg the command
     * @throws InterruptedException if the command could not have been added to the queue
     */
    public void sendCommand(String msg) throws InterruptedException {
        synchronized (commandQueue){
            commandQueue.put(msg);
            selector.wakeup();
        }
    }

    private void processCommands() throws BadLocationException {
        while (true){
            synchronized (commandQueue){
                var msg = commandQueue.poll();
                if (msg == null) return;
                String[] param = msg.split(" ", 2);
                switch (msg.charAt(0)){
                    case '@':
                    	if (param.length != 2) break;
                        String pseudo = param[0].substring(1);
                        if (!accepted.containsKey(pseudo)){
                            var prvtCtx = pendingClient(pseudo);
                            uniqueContext.queueMessage(ClientRequestUtils.privateConnectionRequest(login, pseudo,
                                    serverSocketChannel.socket().getInetAddress().getAddress(),
                                    serverSocketChannel.socket().getLocalPort()));
                            prvtCtx.queueMessage(ClientRequestUtils.messageRequest((byte) 50, new Message(login, param[1])));
                            app.displayPrivateMessage(pseudo, param[1], true);
                        } else {
                        	accepted.get(pseudo).queueMessage(ClientRequestUtils.HealthCheckRequest());
                        	if (accepted.containsKey(pseudo)) {
                        		accepted.get(pseudo).queueMessage(ClientRequestUtils.messageRequest((byte) 50, new Message(login, param[1])));
                        		app.displayPrivateMessage(pseudo, param[1], true);
                        	}
                        }
                        break;
                    case '/':
                    	if (param.length != 2) break;
                    	pseudo = param[0].substring(1);
                    	ByteBuffer[] buffers = ClientRequestUtils.filesRequest((byte) 51, random.nextLong(), login, fileDir, param[1]);
                    	if (buffers == null) {
                    		app.displayFileNotExist(fileDir.resolve(param[1]));
                    		return ;
                    	}
                    	if (!accepted.containsKey(pseudo)){
                            var prvtCtx = pendingClient(pseudo);
                            uniqueContext.queueMessage(ClientRequestUtils.privateConnectionRequest(login, pseudo,
                                    serverSocketChannel.socket().getInetAddress().getAddress(),
                                    serverSocketChannel.socket().getLocalPort()));
                            prvtCtx.queueFile(buffers);
                        	app.displayPrivateFile(pseudo, param[1], true);
                        } else {
                        	accepted.get(pseudo).queueMessage(ClientRequestUtils.HealthCheckRequest());
                        	if (accepted.containsKey(pseudo)) {
                        		accepted.get(pseudo).queueFile(buffers);
                            	app.displayPrivateFile(pseudo, param[1], true);
                        	}
                        }
                        break;
                    case '!':
                        if (param.length < 1 || param.length > 2)
                            return;
                        String cmd = param[0].substring(1);
                        switch (cmd){
                            case "accept":
                                if (param.length == 2 && pending.containsKey(param[1])) doPrivateResponseRequest(login, param[1]);
                                break;
                            case "decline":
                            	if (param.length == 1 || !pending.containsKey(param[1])) return ;
                                var prvtCtx = pending.remove(param[1]);
                                prvtCtx.silentlyClose();
                                uniqueContext.queueMessage(ClientRequestUtils.privateDeclinedConnectionRequest(login, param[1]));
                                break;
                            case "list":
                            	for (var pseudo_prvt : pending.keySet())
                                    app.displayPendingConnection(pseudo_prvt);
                                break;
                        }
                        break;
                    default:
                        uniqueContext.queueMessage(ClientRequestUtils.messageRequest((byte) 30, new Message(login, msg)));
                }
            }
        }
    }
    
    /**
     * method which allows to write a file called filename defined by the data contained in buffers
     * @param buffers some data
     * @param filename the name of the file
     */
    public void writeFile(ByteBuffer[] buffers, String filename) {
    	//check if file exist
    	Path p = fileDir.resolve(filename);
    	if (Files.exists(p)) {
    		File folder = fileDir.toFile();
    		long num = Arrays.stream(folder.listFiles()).count();
    		String[] tokens = filename.split("\\.");
    		p = fileDir.resolve(tokens[0] + "-" + num + "." + tokens[1]);
    	}
    	//write file
    	try(FileChannel fc = FileChannel.open(p, StandardOpenOption.CREATE, StandardOpenOption.WRITE, StandardOpenOption.TRUNCATE_EXISTING)) {
    		fc.write(buffers);
    	} catch (IOException e) {
			e.printStackTrace();
		}
    }

    /**
     * add a client to the pending connection
     * @param pseudo the pseudo of the client who wants to initiate a connection
     * @return the context denoting the client who sends the request
     */
    public PrivateContext pendingClient(String pseudo){
        PrivateContext prvtC = new PrivateContext(this);
        pending.put(pseudo, prvtC);
        return prvtC;
    }
    
    /**
     * retrieve a PrivateContext denoted by the pseudo from the pending connections
     * @param pseudo the pseudo associated with the PrivateContext
     * @return an Optional with a PrivateContext inside if the pseudo is registered
     */
    public Optional<PrivateContext> getClientPrivateContext(String pseudo){
        return Optional.ofNullable(pending.get(pseudo));
    }

    /**
     * method which remove a client from pending connections and add it to the accepted connections
     * @param pseudo the pseudo of the client to move from pending to accepted
     * @return the PrivateContext of the client 'pseudo'
     */
    public PrivateContext acceptedClient(String pseudo){
        var prvtCtx = pending.remove(pseudo);
        accepted.put(pseudo, prvtCtx);
        prvtCtx.setRegistered(true);
        return prvtCtx;
    }

    /**
     * Open the connection of a private context and set its SocketChannel + InetSocketAddress
     * @param ctx the context to set the channel + address
     * @param address the address to associate
     * @throws IOException if there was an issue while opening the socketChannel
     */
    public void openNoRegisterClient(PrivateContext ctx, InetSocketAddress address) throws IOException {
        SocketChannel tmp = SocketChannel.open();
        tmp.configureBlocking(false);
        ctx.setSc(tmp);
        ctx.setInet(address);
    }

    /**Same than the method openNoRegisterClient but register the selectionKey associated to the PrivateContext
     * @param ctx the PrivateContext to register 
     * @param address the address to associate
     * @throws IOException if there was an issue while opening or connecting the socketChannel
     * @see openNoRegisterClient
     */
    public void openAndRegisterClient(PrivateContext ctx, InetSocketAddress address) throws IOException {
        SocketChannel tmp = SocketChannel.open();
        tmp.configureBlocking(false);
        var key = tmp.register(selector, SelectionKey.OP_CONNECT);
        ctx.setKey(key);
        ctx.setInet(address);
        ctx.setRegistered(true);
        tmp.connect(address);
        ctx.setSc(tmp);
        key.attach(ctx);
    }

    /**
     * register the selectionKey associated to the PrivateContext with opening the connection 
     * @param ctx the context to register
     * @throws IOException if there was an issue while connecting the socketChannel
     */
    public void registerNoOpen(PrivateContext ctx) throws IOException {
        var key = ctx.getSc().register(selector, SelectionKey.OP_CONNECT);
        ctx.setKey(key);
        ctx.setRegistered(true);
        key.attach(ctx);
        ctx.getSc().connect(ctx.getInet());
    }

    /**
     * Remove a client with the pseudo 'pseudo' from the pending connections and indicate the connection had been declined
     * @param pseudo the pseudo
     * @throws BadLocationException if there is an issue with the GUI
     */
    public void removePendingClient(String pseudo) throws BadLocationException {
        pending.remove(pseudo);
        app.displayDeclinedConnection(pseudo);
    }
    
    /**
     * Remove a client with the pseudo 'pseudo' from the accepted connections
     * @param prvt the context to remove
     */
    public void removeAcceptedClient(PrivateContext prvt) {
    	String login = accepted.entrySet().stream().filter((entry) -> entry.getValue() == prvt).findFirst().get().getKey();	//beurk
    	accepted.remove(login);
        try {
			app.displayRemovedConnection(login);
		} catch (BadLocationException e) {
			//
		}
	}
    
    /**
     * Remove a client with the pseudo 'pseudo' from the pending connections and indicate the client doesn't exists
     * @param pseudo the pseudo
     * @throws BadLocationException if there is an issue with the GUI
     */
    public void removePendingClientNotExist(String pseudo) throws BadLocationException {
        pending.remove(pseudo);
        app.displayUserNotExistConnection(pseudo);
    }

    /**
     * send a private connection request from client p1 to user p2
     * @param p1 the sender
     * @param p2 the receiver
     */
    public void doPrivateResponseRequest(String p1, String p2){
        uniqueContext.queueMessage(ClientRequestUtils.privateAcceptedConnectionRequest(p1, p2,
                serverSocketChannel.socket().getInetAddress().getAddress(),
                serverSocketChannel.socket().getLocalPort()));
    }
    
    /**
     * close the client, display some informations on the deconnection and close the GUI
     * @param message the message to display
     * @param publicContext the context to close
     */
    public void close(String message, PublicContext publicContext) {
		publicContext.silentlyClose();
		System.out.println("closing : " + message);
		app.dispose();
	}

    /**
     * launch the client and the Thread for the HealthCheck
     * @throws IOException if there is an issue while connecting the SocketChannel of the client
     */
    public void launch() throws IOException {
        serverSocketChannel.configureBlocking(false);
        serverSocketChannel.register(selector, SelectionKey.OP_ACCEPT);

        sc.configureBlocking(false);
        var key = sc.register(selector, SelectionKey.OP_CONNECT);
        uniqueContext = new PublicContext(this, key);
        key.attach(uniqueContext);
        sc.connect(serverAddress);
        
        Thread healthCheck = new Thread(this::healthCheckAccepted);
        healthCheck.setDaemon(true);
        healthCheck.start();

        while(!Thread.interrupted()) {
            //printKeys();
            try {
                selector.select(this::treatKey);
                processCommands();
            } catch (UncheckedIOException tunneled) {
                throw tunneled.getCause();
            } catch (BadLocationException e) {
                e.printStackTrace();
            }
        }
    }

    private void treatKey(SelectionKey key) {
        try {
            if (key.isValid() && key.isAcceptable())
                doAccept(key);
        } catch(IOException ioe) {
            // lambda call in select requires to tunnel IOException
            throw new UncheckedIOException(ioe);
        }
        try {
            if (key.isValid() && key.isConnectable())
            	((Context) key.attachment()).doConnect();
            if (key.isValid() && key.isWritable())
                ((Context) key.attachment()).doWrite();
            if (key.isValid() && key.isReadable())
                ((Context) key.attachment()).doRead();

        } catch (IOException ioe) {
            // lambda call in select requires to tunnel IOException
            throw new UncheckedIOException(ioe);
        }
    }

    private void doAccept(SelectionKey key) throws IOException {
        SocketChannel sc = serverSocketChannel.accept();
        if (sc != null) {
            sc.configureBlocking(false);
            SelectionKey clientKey = sc.register(selector, SelectionKey.OP_READ);
            var prvtCtx = new PrivateContext(this);
            prvtCtx.setKey(clientKey);
            prvtCtx.setSc(sc);
            prvtCtx.setRegistered(true);
            clientKey.attach(prvtCtx);
        }
    }

    /**
     * getter for the authentification
     * @return a boolean which indicates if the connection require an authentification or not
     */
    public boolean isAuth(){
        return auth;
    }

    /**
     * getter for the login
     * @return the login of the client
     */
    public String getLogin(){
        return login;
    }

    /**
     * getter for the password
     * @return the password of the client or null (connection not authentified)
     */
    public String getPassword(){
        return password;
    }

    /**
     * getter for the GUI Object
     * @return the object representing the GUI
     */
    public AppFrame getApp(){
        return app;
    }

    private static void usage(){
        System.out.println("Usage : ChatHack host port path login (password)");
    }

    /**
     * main program to launch the client
     * @param args arguments of the command line
     * @throws IOException if there is an issue while launching the client
     */
    public static void main(String[] args) throws IOException {
    	if (args.length < 4 || args.length > 5) {
    		usage();
    		return ;
    	}
        ClientChatHack client = new ClientChatHack(args);
        client.launch();
    }
}
