/**
 * Provides the classes necessary to instanciate a client for the ChatHack protocol
 */
package fr.uge.client;