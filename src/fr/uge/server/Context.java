package fr.uge.server;

import java.io.IOException;

/**
 * interface made to facilitate the read and write operations of the selectionKey registered by the selector of a server
 */
public interface Context {
	/**
	 * performs a read operation
	 * @throws IOException if there is a problem while reading
	 */
	void doRead() throws IOException;
	
	/**
	 * performs a write operation
	 * @throws IOException if there is a problem while writing
	 */
	void doWrite() throws IOException;
}
