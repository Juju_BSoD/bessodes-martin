package fr.uge.server;

import java.nio.ByteBuffer;
import java.nio.charset.Charset;
import java.nio.charset.StandardCharsets;

import fr.uge.reader.Connection;
import fr.uge.reader.ConnectionAborted;
import fr.uge.reader.Message;

/**
 * A class containing some useful methods to builds requests, look at our RFC for some details on the protocol ChatHack
 */
public class ServerRequestUtils {

    private static final Charset UTF8 = StandardCharsets.UTF_8;

	/**
	 * a method to build request with only one byte
	 * @param b the byte to include
	 * @return the request well-formed
	 */
	public static ByteBuffer buildByteBufferWithByte(byte b){
        ByteBuffer bb = ByteBuffer.allocate(1);
        bb.put(b);
        return bb;
    }
	
	/**
	 * a method to build the message request
	 * @param msg the message to send
	 * @return the request well-formed
	 */
	public static ByteBuffer buildMessageRequest(Message msg){
		var bbtmp = ServerRequestUtils.buildByteBufferWithByte((byte) 30).flip();
		var bbmsg = msg.toByteBuffer().flip();
		var bb = ByteBuffer.allocate(Byte.BYTES + bbmsg.remaining());
		bb.put(bbtmp);
		return bb.put(bbmsg);
    }


	/**
	 * a method to build a request that tells the user doesn't exists
	 * @param pseudo the pseudo
	 * @return the request well-formed
	 */
    public static ByteBuffer buildNotExistRequest(String pseudo){
    	ByteBuffer pseudobb = UTF8.encode(pseudo);
        ByteBuffer bb = ByteBuffer.allocate(Byte.BYTES + Integer.BYTES + pseudobb.remaining());
        bb.put((byte) 41).putInt(pseudobb.remaining()).put(pseudobb);
        return bb;
    }

    /**
	 * a method to build a connexion request
	 * @param cnt the Connection object
	 * @return the request well-formed
	 */
    public static ByteBuffer buildSpreadPrivateConnectionRequest(Connection cnt){
	    ByteBuffer tmp = cnt.toByteBuffer().flip();
	    ByteBuffer bb = ByteBuffer.allocate(Byte.BYTES + tmp.remaining());
	    bb.put((byte)40).put(tmp);
	    return bb;
    }

    /**
	 * a method to build a positive answer connection request from a Connection object
	 * @param cnt the Connection object
	 * @return the request well-formed
	 */
    public static ByteBuffer buildSpreadPrivateAcceptedConnectionRequest(Connection cnt){
        ByteBuffer tmp = cnt.toByteBuffer().flip();
        ByteBuffer bb = ByteBuffer.allocate(Byte.BYTES + tmp.remaining());
        bb.put((byte)42).put(tmp);
        return bb;
    }

    /**
	 * a method to build a negative answer connection request from a ConnectionBorted object
	 * @param ca the ConnectionAborted object
	 * @return the request well-formed
	 */
    public static ByteBuffer buildSpreadPrivateDeclinedConnectionRequest(ConnectionAborted ca){
        ByteBuffer tmp = ca.toByteBuffer().flip();
        ByteBuffer bb = ByteBuffer.allocate(Byte.BYTES + tmp.remaining());
        bb.put((byte)43).put(tmp);
        return bb;
    }

}
