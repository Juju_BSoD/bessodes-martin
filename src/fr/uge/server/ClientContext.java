package fr.uge.server;

import java.io.IOException;
import java.nio.ByteBuffer;
import java.nio.channels.SelectionKey;
import java.nio.channels.SocketChannel;
import java.util.LinkedList;
import java.util.Queue;

import fr.uge.reader.Authentification;
import fr.uge.reader.Connection;
import fr.uge.reader.ConnectionAborted;
import fr.uge.reader.Message;
import fr.uge.reader.PacketReader;
import fr.uge.reader.Reader;

/**
 * context class which represents a client accepted by the server
 */
class ClientContext implements Context {
	
    private final SelectionKey key;
    private final SocketChannel sc;
    private final static int BUFFER_SIZE = 10_000;
    private final ByteBuffer bbin = ByteBuffer.allocate(BUFFER_SIZE);
    private final ByteBuffer bbout = ByteBuffer.allocate(BUFFER_SIZE);
    private final Queue<ByteBuffer> queue = new LinkedList<>(); // buffers read-mode
    private final PacketReader packetReader = new PacketReader();
    private final ServerChatHack sch;
    private boolean closed = false;
    private boolean connected = false;
    private String login;

    public ClientContext(ServerChatHack sch, SelectionKey key){
        this.key = key;
        this.sc = (SocketChannel) key.channel();
        this.sch = sch;
    }
    
    private void treatPacket(PacketReader reader) {
    	switch (packetReader.getOpCode()) {
			case 10 :
				Authentification auth = (Authentification) packetReader.get();
				sch.doRequestPublic(auth, this);
				break;
			case 20 :
				auth = (Authentification) packetReader.get();
				sch.doRequestPrivate(auth, this);
				break;
			case 30 :
				Message msg = (Message) packetReader.get();
				if (msg.getLogin().equals(login)) sch.broadcast(msg);
				else silentlyClose();
				break;
			case 40 :
                Connection cnt = (Connection) packetReader.get();
                if (sch.hasConnectedClient(cnt.getPseudo2()) && !cnt.getPseudo1().equals(cnt.getPseudo2())) {
                	sch.doSpecificRequest(cnt.getPseudo2(), ServerRequestUtils.buildSpreadPrivateConnectionRequest(cnt));
                }
                else
                    queueMessage(ServerRequestUtils.buildNotExistRequest(cnt.getPseudo2()));
				break;
			case 42 :
                cnt = (Connection) packetReader.get();
                sch.doSpecificRequest(cnt.getPseudo2(), ServerRequestUtils.buildSpreadPrivateAcceptedConnectionRequest(cnt));
				break;
			case 43 :
				ConnectionAborted ca = (ConnectionAborted) packetReader.get();
				sch.doSpecificRequest(ca.getPseudo2(), ServerRequestUtils.buildSpreadPrivateDeclinedConnectionRequest(ca));
				break;
			default :
				throw new AssertionError();
		}
    }

    /**
     * Process the content of bbin
     *
     * The convention is that bbin is in write-mode before the call
     * to process and after the call
     *
     */
    private void processIn() {
    	for(;;) {
    		Reader.ProcessStatus status = packetReader.process(bbin);
    		switch (status) {
    	    	case DONE:
    	    		treatPacket(packetReader);
    	    		System.out.println(packetReader.getOpCode() + " " + packetReader.get());
    	    		packetReader.reset();
    	    		break;
    	    	case REFILL:
    	    		return;
    	    	case ERROR:
    	    		System.out.println("error, closing connection");
    	    		silentlyClose();
    	    		return;
    	    }
    	}
    }

    /**
     * Add a message to the message queue, tries to fill bbOut and updateInterestOps
     *
     * @param bb
     */
    public void queueMessage(ByteBuffer bb) {
        queue.add(bb);
        processOut();
        updateInterestOps();
    }

    /**
     * Try to fill bbout from the message queue
     *
     */
    private void processOut() {
        while (!queue.isEmpty()){
            var bb = queue.peek().flip();
            if (bb.remaining()<=bbout.remaining()){
                queue.remove();
                bbout.put(bb);
            }
            else return ;
        }
    }

    /**
     * Update the interestOps of the key looking
     * only at values of the boolean closed and
     * of both ByteBuffers.
     *
     * The convention is that both buffers are in write-mode before the call
     * to updateInterestOps and after the call.
     * Also it is assumed that process has been be called just
     * before updateInterestOps.
     */

    private void updateInterestOps() {
        var interesOps=0;
        if (!closed && bbin.hasRemaining())
            interesOps=interesOps|SelectionKey.OP_READ;
        if (bbout.position()!=0)
            interesOps|=SelectionKey.OP_WRITE;
        if (interesOps==0){
        	silentlyClose();
            return;
        }
        key.interestOps(interesOps);
    }

    public void silentlyClose() {
        try {
        	if (isConnected()) sch.disconnect(login);
            sc.close();
        } catch (IOException e) {
            // ignore exception
        }
    }

    /**
     * Performs the read action on sc
     *
     * The convention is that both buffers are in write-mode before the call
     * to doRead and after the call
     *
     * @throws IOException
     */
    public void doRead() throws IOException {
        if (sc.read(bbin)==-1)
            closed=true;
        processIn();
        updateInterestOps();
    }

    /**
     * Performs the write action on sc
     *
     * The convention is that both buffers are in write-mode before the call
     * to doWrite and after the call
     *
     * @throws IOException
     */

    public void doWrite() throws IOException {
        bbout.flip();
        sc.write(bbout);
        bbout.compact();
        processOut();
        updateInterestOps();
    }
    
    public void connect(String login) {
    	connected = true;
    	this.login = login;
    }
    
    public boolean isConnected() {
    	return connected;
    }
    
    public String getLogin() {
    	if (!isConnected()) throw new IllegalStateException("client not connected");
    	return login;
    }
}
