package fr.uge.server;

import java.io.IOException;
import java.io.UncheckedIOException;
import java.net.InetSocketAddress;
import java.net.SocketAddress;
import java.nio.ByteBuffer;
import java.nio.channels.Channel;
import java.nio.channels.SelectableChannel;
import java.nio.channels.SelectionKey;
import java.nio.channels.Selector;
import java.nio.channels.ServerSocketChannel;
import java.nio.channels.SocketChannel;
import java.nio.charset.Charset;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.Set;
import java.util.logging.Level;
import java.util.logging.Logger;

import fr.uge.reader.AnswerAuthentification;
import fr.uge.reader.Authentification;
import fr.uge.reader.Message;

/**
 * A class representing the server which can accepts ChatHack's client
 */
public class ServerChatHack {
	
	static private int BUFFER_SIZE = 1024;
	static private Logger logger = Logger.getLogger(ServerChatHack.class.getName());
	static private Charset UTF8 = Charset.forName("utf8");

	private final ServerSocketChannel serverSocketChannel;
	private final SelectionKey mdp;
	private final HashMap<String, ClientContext> clients = new HashMap<>();
	private final HashMap<Long, String> connections = new HashMap<>();
	private final Selector selector;
	private Long nextId = 1L;

	private ServerChatHack(int port, SocketChannel scMDP) throws IOException {
		serverSocketChannel = ServerSocketChannel.open();
		serverSocketChannel.bind(new InetSocketAddress(port));
		selector = Selector.open();
		scMDP.configureBlocking(false);
		mdp = scMDP.register(selector, SelectionKey.OP_READ);
		MDPContext ctx = new MDPContext(this, mdp);
		mdp.attach(ctx);
	}
	
	/**
	 * factory method to init the server
	 * @param port the port to exposer the server on
	 * @param portMDP the port of the password's server
	 * @return the newly created server
	 * @throws IOException if there is a problem while connecting to the password's server
	 */
	public static ServerChatHack create(int port, int portMDP) throws IOException {
		if (port == portMDP) throw new IllegalArgumentException("port(" + port + ") must be different than portMDP(" + portMDP + ")");
		SocketChannel sc = initServerSocketMdp(portMDP);
		ServerChatHack server = new ServerChatHack(port, sc);
		return server;
	}
	
	private static SocketChannel initServerSocketMdp(int portMDP) {
		SocketChannel sc = null;
		SocketAddress serverAddress = new InetSocketAddress(portMDP);
		try {
			sc = SocketChannel.open(serverAddress);
		} catch (IOException e) {
			logger.severe("error while connecting to the ServerMDP, please be sure to turn on the server");
			e.printStackTrace();
			System.exit(1);
		}
		return sc;
	}
	
	/**
	 * send to the password's server a private authentification request
	 * @param auth the Authentification object
	 * @param client the context of the client
	 */
	public void doRequestPrivate(Authentification auth, ClientContext client) {
		if (clients.containsKey(auth.getLogin())) {	//send error to client
			ByteBuffer bb = ServerRequestUtils.buildByteBufferWithByte((byte) 23);
			client.queueMessage(bb);
			return ;
		}
		clients.put(auth.getLogin(), client);
		//queue the private connection request to the mdp context
		auth.setID(nextId++);
		connections.put(auth.getID(), auth.getLogin());
		((MDPContext) mdp.attachment()).queueAuthentification(auth);
	}

	/**
	 * send to the password's server a public authentification request
	 * @param auth the Authentification object
	 * @param client the context of the client
	 */
	public void doRequestPublic(Authentification auth, ClientContext client) {
		//System.out.println("login : " + auth.getLogin() + ", exists : " + clients.containsKey(auth.getLogin()));
		if (clients.containsKey(auth.getLogin())) {	//send error to client
			ByteBuffer bb = ServerRequestUtils.buildByteBufferWithByte((byte) 12);
			client.queueMessage(bb);
			return ;
		}
		clients.put(auth.getLogin(), client);
		//queue the public connection request to the mdp context
		auth.setID(nextId++);
		connections.put(auth.getID(), auth.getLogin());
		((MDPContext) mdp.attachment()).queueAuthentification(auth);
	}

	
	/**
	 * send a specific request (bb) to a connected client identified by its pseudo
	 * @param pseudo the pseudo of the client
	 * @param bb the request
	 */
	public void doSpecificRequest(String pseudo, ByteBuffer bb){
		if (clients.containsKey(pseudo)) clients.get(pseudo).queueMessage(bb);
	}
	
	/**
	 * notify the client according to the answer of the password's server
	 * @param answer the answer received from the password's server
	 */
	public void notifyClient(AnswerAuthentification answer) {
		String login;
		ClientContext client;
		ByteBuffer bb;
		if ((login = connections.get(answer.getID())) == null) throw new IllegalStateException("unknown login");
		if ((client = clients.get(login)) == null) throw new IllegalStateException("unknown client");
		
		if (answer.isCredentialExists() && answer.isPrivateAuth()) {
			bb = ServerRequestUtils.buildByteBufferWithByte((byte) 21);
			client.connect(login);
		} else if (answer.isCredentialExists() && !answer.isPrivateAuth()) {
			bb = ServerRequestUtils.buildByteBufferWithByte((byte) 12);
			disconnect(login);
		} else if (!answer.isCredentialExists() && answer.isPrivateAuth()) {
			bb = ServerRequestUtils.buildByteBufferWithByte((byte) 22);
			disconnect(login);
		} else {
			bb = ServerRequestUtils.buildByteBufferWithByte((byte) 11);
			client.connect(login);
		}
		client.queueMessage(bb);
	}

	/**
	 * tells whether a client with the pseudo 'pseudo' is connected
	 * @param pseudo the pseudo to check
	 * @return a boolean which indicates if the client exists and if the client is connected
	 */
	public boolean hasConnectedClient(String pseudo){
		return clients.containsKey(pseudo) && clients.get(pseudo).isConnected();
	}
	
	/**
	 * remove the client with the login
	 * @param login the login which identifies the client
	 */
	public void disconnect(String login) {
		clients.remove(login);
	}
	
	/**
	 * broadcast a message to all the connected clients
	 * @param msg the message
	 */
	public void broadcast(Message msg) {
    	var keys = selector.keys();
    	
    	var bb = ServerRequestUtils.buildMessageRequest(msg);
    	for (SelectionKey key : keys) {
    		if (key.attachment() instanceof ClientContext) {
    			var context = (ClientContext) key.attachment();
    			context.queueMessage(bb);
    		}
    	}
    }
	
	/**
	 * launch the server
	 * @throws IOException if there is a problem while treating a selection key
	 */
	public void launch() throws IOException {
		serverSocketChannel.configureBlocking(false);
		serverSocketChannel.register(selector, SelectionKey.OP_ACCEPT);
		while (!Thread.interrupted()) {
			//printKeys(); // for debug
			//System.out.println("Starting select");
			try {
				selector.select(this::treatKey);
			} catch (UncheckedIOException tunneled) {
				throw tunneled.getCause();
			}
			//System.out.println("Select finished");
		}
	}
	
	private void treatKey(SelectionKey key) {
		//printSelectedKey(key); // for debug
		try {
			if (key.isValid() && key.isAcceptable())
				doAccept(key);
		} catch(IOException ioe) {
			// lambda call in select requires to tunnel IOException
			throw new UncheckedIOException(ioe);
		}
		try {
			if (key.isValid() && key.isWritable())
				((Context) key.attachment()).doWrite();
			if (key.isValid() && key.isReadable())
				((Context) key.attachment()).doRead();
		} catch (IOException e) {
			logger.log(Level.INFO,"Connection closed with client due to IOException",e);
			silentlyClose(key);
			if (key.attachment() instanceof ClientContext) {
    			var context = (ClientContext) key.attachment();
    			if (context.isConnected()) disconnect(context.getLogin());
    		}
		}
	}
	
	private void doAccept(SelectionKey key) throws IOException {
    	SocketChannel sc = serverSocketChannel.accept();
    	if (sc != null) {
    		sc.configureBlocking(false);
    		SelectionKey clientKey = sc.register(selector, SelectionKey.OP_READ);
    		clientKey.attach(new ClientContext(this, clientKey));
    	}
    }
	
	private void silentlyClose(SelectionKey key) {
        Channel sc = (Channel) key.channel();
        try {
            sc.close();
        } catch (IOException e) {
            // ignore IOException
        }
    }
	
	private static void usage(){
		System.out.println("Usage : ServerChatHack port port_MDP");
	}
	
	/**
     * main program to launch the server
     * @param args arguments of the command line
     * @throws IOException if there is an issue while launching the client
     * @throws NumberFormatException if one of the number written on the command line is not formated as expected
     */
	public static void main(String[] args) throws NumberFormatException, IOException {
		if (args.length!=2){
			usage();
			return;
		}
		ServerChatHack server = ServerChatHack.create(Integer.parseInt(args[0]), Integer.parseInt(args[1]));
		System.out.println("ChatHack server created succesfully");
		server.launch();
	}
}
