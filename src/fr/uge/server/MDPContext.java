package fr.uge.server;

import java.io.IOException;
import java.nio.ByteBuffer;
import java.nio.channels.SelectionKey;
import java.nio.channels.SocketChannel;
import java.nio.charset.Charset;
import java.util.HashMap;
import java.util.LinkedList;
import java.util.Queue;

import fr.uge.reader.AnswerAuthentification;
import fr.uge.reader.AnswerAuthentificationReader;
import fr.uge.reader.Authentification;
import fr.uge.reader.Reader;

/**
 * context class which represents the password's database
 */
class MDPContext implements Context {
	
	private final Charset UTF8 = Charset.forName("utf8");
	
	private final SelectionKey key;
    private final SocketChannel sc;
    private final int BUFFER_SIZE = 5000;
    private final ByteBuffer bbin = ByteBuffer.allocate(BUFFER_SIZE);
    private final ByteBuffer bbout = ByteBuffer.allocate(BUFFER_SIZE);
    private final Queue<ByteBuffer> queue = new LinkedList<>(); // buffers read-mode
    private final HashMap<Long, Boolean> isPrivateConnect = new HashMap<>();	//for a request keep in memory if it was public or private connection
    private AnswerAuthentificationReader answerReader = new AnswerAuthentificationReader();
    private final ServerChatHack sch;
    private boolean closed = false;
    
    public MDPContext(ServerChatHack sch, SelectionKey key){
        this.key = key;
        this.sc = (SocketChannel) key.channel();
        this.sch = sch;
    }/**
     * Process the content of bbin
    *
    * The convention is that bbin is in write-mode before the call
    * to process and after the call
    *
    */
	private void processIn() {
	   	for(;;) {
	   		Reader.ProcessStatus status = answerReader.process(bbin);
	   		switch (status) {
	   	    	case DONE:
	   	    		System.out.println(answerReader.get());
	   	    		AnswerAuthentification answer = answerReader.get();
	   	    		boolean isPrivate = isPrivateConnect.get(answer.getID());
	   	    		answer.setPrivateConnect(isPrivate);
	   	    		sch.notifyClient(answer);
	   	    		answerReader.reset();
	   	    		break;
	   	    	case REFILL:
	   	    		return;
	   	    	case ERROR:
	   	    		System.out.println("error, closing connection");
	   	    		silentlyClose();
	   	    		return;
	   	    }
	   	}
	}

   /**
    * Add a message to the message queue, tries to fill bbOut and updateInterestOps
    *
    * @param bb
    */
   public void queueAuthentification(Authentification auth) {
	   isPrivateConnect.put(auth.getID(), auth.isPrivateAuthentification());
	   ByteBuffer bb = auth.toByteBuffer();
       queue.add(bb);
       processOut();
       updateInterestOps();
   }

   /**
    * Try to fill bbout from the message queue
    *
    */
   private void processOut() {
       while (!queue.isEmpty()){
           var bb = queue.peek().flip();
           if (bbout.position() == 0) {
               queue.remove();
               bbout.put(bb);
           }
           else return ;
       }
   }

   /**
    * Update the interestOps of the key looking
    * only at values of the boolean closed and
    * of both ByteBuffers.
    *
    * The convention is that both buffers are in write-mode before the call
    * to updateInterestOps and after the call.
    * Also it is assumed that process has been be called just
    * before updateInterestOps.
    */

   private void updateInterestOps() {
	   var interesOps=0;
       if (!closed && bbin.position()==0)
           interesOps=interesOps|SelectionKey.OP_READ;
       if (bbout.position()!=0)
           interesOps|=SelectionKey.OP_WRITE;
       if (interesOps==0){
           silentlyClose();
           return;
       }
       key.interestOps(interesOps);
   }

   private void silentlyClose() {
       try {
           sc.close();
       } catch (IOException e) {
           // ignore exception
       }
   }

   /**
    * Performs the read action on sc
    *
    * The convention is that both buffers are in write-mode before the call
    * to doRead and after the call
    *
    * @throws IOException
    */
   public void doRead() throws IOException {
       if (sc.read(bbin)==-1)
           closed=true;
       processIn();
       updateInterestOps();
   }

   /**
    * Performs the write action on sc
    *
    * The convention is that both buffers are in write-mode before the call
    * to doWrite and after the call
    *
    * @throws IOException
    */

   public void doWrite() throws IOException {
       bbout.flip();
       sc.write(bbout);
       bbout.compact();
       processOut();
       updateInterestOps();
   }
}