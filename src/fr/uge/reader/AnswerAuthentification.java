package fr.uge.reader;

/**
 * A class to store the arguments of the answer authentification request
 */
public class AnswerAuthentification {
	private boolean exists;
	private long ID;
	private boolean privateAuth;
	
	/**
	 * instanciate the answer
	 * @param exists tells if the login exists or not
	 * @param ID the ID of the request
	 */
	public AnswerAuthentification(boolean exists, long ID) {
		this.exists = exists;
		this.ID = ID;
	}

	/**
	 * getter for the boolean exists
	 * @return the value of exists
	 */
	public boolean isCredentialExists() {
		return exists;
	}

	/**
	 * getter for the ID
	 * @return the ID
	 */
	public long getID() {
		return ID;
	}

	/**
	 * getter for the boolean privateConnect
	 * @return the value of privateConnect
	 */
	public boolean isPrivateAuth() {
		return privateAuth;
	}

	/**
	 * setter for the privateConnect field
	 * @param privateAuth tells whether this is a private authentification or not
	 */
	public void setPrivateConnect(boolean privateAuth) {
		this.privateAuth = privateAuth;
	}

	/**
	 * display the AnswerAuthentification object
	 */
	@Override
	public String toString() {
		return "AnswerAuthentification [answer=" + exists + ", ID=" + ID + "]";
	}
}
