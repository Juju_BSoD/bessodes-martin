package fr.uge.reader;

import java.nio.ByteBuffer;

/**
 * Reader performing a decode process for the authentification request
 */
public class AuthentificationReader implements Reader<Authentification> {
	
	private enum State {DONE,WAITING_LOGIN,WAITING_PASS,ERROR};
	
	private State state = State.WAITING_LOGIN;
	private final StringReader stringReader = new StringReader(StringReader.Type.BYT);
	private boolean seekingForPassword;
	private boolean processLaunched = false;
    private String login = null;
    private String pass = null;
    
    /**
     * Create an AuthentificationReader
     * @param seekingForPassword tells if the Reader needs to seek for the password or not in the buffer
     */
    public AuthentificationReader(boolean seekingForPassword) {
    	this.seekingForPassword = seekingForPassword;
    }
    
    /**
     * setter for the field seekingForPassword
     * @param seekingForPassword tells if the Reader needs to seek for the password or not in the buffer
     */
    public void setseekingForPassword(boolean seekingForPassword) {
    	if (processLaunched) throw new IllegalStateException("process have been launched, you can not do that");
    	this.seekingForPassword = seekingForPassword;
    }
    
    /**
     * {@inheritDoc}
     */
    @Override
	public ProcessStatus process(ByteBuffer bb) {
    	processLaunched = true;
    	if (state== State.DONE || state== State.ERROR) {
            throw new IllegalStateException();
        }
		switch (state) {
			case WAITING_LOGIN :
				ProcessStatus status = stringReader.process(bb);
				if (status == ProcessStatus.REFILL) {
					return status;
				}
				if (status == ProcessStatus.ERROR) {
					state = State.ERROR;
					return status;
				}
				login = stringReader.get();
				if (!seekingForPassword) {
					state = State.DONE;
					return ProcessStatus.DONE;
				}
				state = State.WAITING_PASS;
				stringReader.reset();
			case WAITING_PASS :
				status = stringReader.process(bb);
				if (status == ProcessStatus.REFILL) {
					return status;
				}
				if (status == ProcessStatus.ERROR) {
					state = State.ERROR;
					return status;
				}
				pass = stringReader.get();
				state = State.DONE;
				return ProcessStatus.DONE;
			default:
				throw new AssertionError();
		}
	}

    /**
     * {@inheritDoc}
     */
	@Override
	public Authentification get() {
		if (state != State.DONE) {
            throw new IllegalStateException();
        }
        return new Authentification(login, pass);
	}

	/**
     * {@inheritDoc}
     */
	@Override
	public void reset() {
		state = State.WAITING_LOGIN;
        stringReader.reset();
        login = null;
        pass = null;
        processLaunched = false;
	}
}
