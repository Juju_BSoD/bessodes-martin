package fr.uge.reader;

import java.nio.ByteBuffer;

/**
 * Reader performing a decode process for the answer authentification request
 */
public class AnswerAuthentificationReader implements Reader<AnswerAuthentification> {
	
	private enum State {DONE,WAITING_ANSWER,WAITING_ID,ERROR};
	
	private State state = State.WAITING_ANSWER;
	private LongReader longReader = new LongReader();
	private ByteReader byteReader = new ByteReader();
	private byte answer;
	private long ID;
	
    /**
     * {@inheritDoc}
     */
    @Override
	public ProcessStatus process(ByteBuffer bb) {
    	if (state== State.DONE || state== State.ERROR) {
            throw new IllegalStateException();
        }
		switch (state) {
			case WAITING_ANSWER :
				ProcessStatus status = byteReader.process(bb);
				if (status == ProcessStatus.REFILL) {
					return status;
				}
				if (status == ProcessStatus.ERROR) {
					state = State.ERROR;
					return status;
				}
				answer = byteReader.get();
				state = State.WAITING_ID;
			case WAITING_ID :
				status = longReader.process(bb);
				if (status == ProcessStatus.REFILL) {
					return status;
				}
				if (status == ProcessStatus.ERROR) {
					state = State.ERROR;
					return status;
				}
				ID = longReader.get();
				state = State.DONE;
				return ProcessStatus.DONE;
			default:
				throw new AssertionError();
		}
	}

    /**
     * {@inheritDoc}
     */
	@Override
	public AnswerAuthentification get() {
		if (state != State.DONE) {
            throw new IllegalStateException();
        }
        return new AnswerAuthentification(answer == (byte)1, ID);
	}

	/**
     * {@inheritDoc}
     */
	@Override
	public void reset() {
		state = State.WAITING_ANSWER;
        byteReader.reset();
        longReader.reset();
        answer = 0;
        ID = 0L;
	}
}
