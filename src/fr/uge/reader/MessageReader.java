package fr.uge.reader;

import java.nio.ByteBuffer;

/**
 * Reader performing a decode process for the message request (public or private)
 */
class MessageReader implements Reader<Message> {
	
	private enum State {DONE,WAITING_LOGIN,WAITING_TEXT,ERROR};
	
	private State state = State.WAITING_LOGIN;
	private final StringReader loginReader = new StringReader(StringReader.Type.BYT);
	private final StringReader textReader = new StringReader(StringReader.Type.INT);
    private String login;
    private String text;
    
    /**
     * {@inheritDoc}
     */
    @Override
	public ProcessStatus process(ByteBuffer bb) {
    	if (state== State.DONE || state== State.ERROR) {
            throw new IllegalStateException();
        }
		switch (state) {
			case WAITING_LOGIN :
				ProcessStatus status = loginReader.process(bb);
				if (status == ProcessStatus.REFILL) {
					return status;
				}
				if (status == ProcessStatus.ERROR) {
					state = State.ERROR;
					return status;
				}
				login = loginReader.get();
				state = State.WAITING_TEXT;
			case WAITING_TEXT :
				status = textReader.process(bb);
				if (status == ProcessStatus.REFILL) {
					return status;
				}
				if (status == ProcessStatus.ERROR) {
					state = State.ERROR;
					return status;
				}
				text = textReader.get();
				state = State.DONE;
				return ProcessStatus.DONE;
			default:
				throw new AssertionError();
		}
	}

    /**
     * {@inheritDoc}
     */
	@Override
	public Message get() {
		if (state != State.DONE) {
            throw new IllegalStateException();
        }
        return new Message(login, text);
	}

	/**
     * {@inheritDoc}
     */
	@Override
	public void reset() {
		state = State.WAITING_LOGIN;
        loginReader.reset();
        textReader.reset();
        login = null;
        text = null;
	}
}
