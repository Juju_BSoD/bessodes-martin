package fr.uge.reader;

import java.nio.ByteBuffer;

/**
 * Reader performing a decode process to get an Integer
 */
public class IntReader implements Reader<Number> {

    private enum State {DONE,WAITING,ERROR};

    private State state = State.WAITING;
    private final ByteBuffer internalbb = ByteBuffer.allocate(Integer.BYTES); // write-mode
    private int value;

    /**
     * {@inheritDoc}
     */
    @Override
    public ProcessStatus process(ByteBuffer bb) {
        if (state== State.DONE || state== State.ERROR) {
            throw new IllegalStateException();
        }
        bb.flip();
        try {
        	if (bb.remaining()<=internalbb.remaining()){
                internalbb.put(bb);
            } else {
                var oldLimit = bb.limit();
                bb.limit(internalbb.remaining());
                internalbb.put(bb);
                bb.limit(oldLimit);
            }
        } finally {
            bb.compact();
        }
        if (internalbb.hasRemaining()){
            return ProcessStatus.REFILL;
        }
        state=State.DONE;
        internalbb.flip();
        value=internalbb.getInt();
        return ProcessStatus.DONE;
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public Integer get() {
        if (state!= State.DONE) {
            throw new IllegalStateException();
        }
        return value;
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public void reset() {
        state= State.WAITING;
        internalbb.clear();
    }
}
