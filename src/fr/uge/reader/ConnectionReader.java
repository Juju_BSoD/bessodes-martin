package fr.uge.reader;

import java.net.InetSocketAddress;
import java.nio.ByteBuffer;

/**
 * Reader performing a decode process for the connection request
 */
public class ConnectionReader implements Reader<Connection> {
	
	private enum State {DONE,WAITING_PSEUDO1,WAITING_PSEUDO2,WAITING_ADDRESS,ERROR};
	
	private State state = State.WAITING_PSEUDO1;
	private final StringReader stringReader = new StringReader(StringReader.Type.BYT);
	private final SocketReader socketReader = new SocketReader();
    private String pseudo1 = null;
    private String pseudo2 = null;
    private InetSocketAddress address = null;
    
    /**
     * {@inheritDoc}
     */
    @Override
	public ProcessStatus process(ByteBuffer bb) {
    	if (state== State.DONE || state== State.ERROR) {
            throw new IllegalStateException();
        }
    	switch (state) {
			case WAITING_PSEUDO1 :
				ProcessStatus status = stringReader.process(bb);
				if (status == ProcessStatus.REFILL) {
					return status;
				}
				if (status == ProcessStatus.ERROR) {
					state = State.ERROR;
					return status;
				}
				pseudo1 = stringReader.get();
				state = State.WAITING_PSEUDO2;
				stringReader.reset();
			case WAITING_PSEUDO2 :
				status = stringReader.process(bb);
				if (status == ProcessStatus.REFILL) {
					return status;
				}
				if (status == ProcessStatus.ERROR) {
					state = State.ERROR;
					return status;
				}
				pseudo2 = stringReader.get();
				state = State.WAITING_PSEUDO2;
			case WAITING_ADDRESS :
				status = socketReader.process(bb);
				if (status == ProcessStatus.REFILL) {
					return status;
				}
				if (status == ProcessStatus.ERROR) {
					state = State.ERROR;
					return status;
				}
				address = socketReader.get();
				state = State.DONE;
				return ProcessStatus.DONE;
			default:
				throw new AssertionError();
		}
	}

    /**
     * {@inheritDoc}
     */
	@Override
	public Connection get() {
		if (state != State.DONE) {
            throw new IllegalStateException();
        }
        return new Connection(pseudo1, pseudo2, address);
	}

	/**
     * {@inheritDoc}
     */
	@Override
	public void reset() {
		state = State.WAITING_PSEUDO1;
        stringReader.reset();
        socketReader.reset();
        pseudo1 = null;
        pseudo2 = null;
        address = null;
	}
}
