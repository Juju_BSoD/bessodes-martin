package fr.uge.reader;

import java.nio.ByteBuffer;

/**
 * Reader performing a decode process to get a ByteBuffer with size Bytes decoded
 */
public class ByteBufferReader implements Reader<ByteBuffer> {

    private enum State {DONE,WAITING,ERROR};

    private State state = State.WAITING;
    private ByteBuffer internalbb;
    private int size = -1;
    private boolean processLaunched = false;
    
    /**
     * method to set the size of the data to retrieve
     * @param size the new size
     */
    public void setSize(int size) {
    	if (size > 1024 || processLaunched) throw new IllegalStateException("size must be less than 1024 : " + size + "or process might have been called before calling this method : " + processLaunched);
    	this.size = size;
    	internalbb = ByteBuffer.allocate(size); // write-mode
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public ProcessStatus process(ByteBuffer bb) {
    	processLaunched = true;
        if (state== State.DONE || state== State.ERROR||size==-1) {
        	throw new IllegalStateException();
        }
        bb.flip();
        try {
        	if (bb.remaining()<=internalbb.remaining()){
                internalbb.put(bb);
            } else {
                var oldLimit = bb.limit();
                bb.limit(internalbb.remaining());
                internalbb.put(bb);
                bb.limit(oldLimit);
            }
        } finally {
            bb.compact();
        }
        if (internalbb.hasRemaining()){
            return ProcessStatus.REFILL;
        }
        state=State.DONE;
        internalbb.flip();
        return ProcessStatus.DONE;
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public ByteBuffer get() {
        if (state!= State.DONE) {
            throw new IllegalStateException();
        }
        ByteBuffer clone = ByteBuffer.allocate(internalbb.capacity());		//create a defensive copy
        clone.put(internalbb);
        internalbb.rewind();
        return clone;
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public void reset() {
        state= State.WAITING;
        internalbb = null;
        size = -1;
        processLaunched = false;
    }
}
