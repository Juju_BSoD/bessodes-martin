package fr.uge.reader;

import java.nio.ByteBuffer;

/**
 * Reader performing a decode process for the connection aborted request
 */
class ConnectionAbortedReader implements Reader<ConnectionAborted> {
	
	private enum State {DONE,WAITING_PSEUDO1,WAITING_PSEUDO2,ERROR};
	
	private State state = State.WAITING_PSEUDO1;
	private final StringReader pseudoReader = new StringReader(StringReader.Type.BYT);
    private String pseudo1;
    private String pseudo2;
    
    /**
     * {@inheritDoc}
     */
    @Override
	public ProcessStatus process(ByteBuffer bb) {
    	if (state== State.DONE || state== State.ERROR) {
            throw new IllegalStateException();
        }
		switch (state) {
			case WAITING_PSEUDO1 :
				ProcessStatus status = pseudoReader.process(bb);
				if (status == ProcessStatus.REFILL) {
					return status;
				}
				if (status == ProcessStatus.ERROR) {
					state = State.ERROR;
					return status;
				}
				pseudo1 = pseudoReader.get();
				pseudoReader.reset();
				state = State.WAITING_PSEUDO2;
			case WAITING_PSEUDO2 :
				status = pseudoReader.process(bb);
				if (status == ProcessStatus.REFILL) {
					return status;
				}
				if (status == ProcessStatus.ERROR) {
					state = State.ERROR;
					return status;
				}
				pseudo2 = pseudoReader.get();
				state = State.DONE;
				return ProcessStatus.DONE;
			default:
				throw new AssertionError();
		}
	}

    /**
     * {@inheritDoc}
     */
	@Override
	public ConnectionAborted get() {
		if (state != State.DONE) {
            throw new IllegalStateException();
        }
        return new ConnectionAborted(pseudo1, pseudo2);
	}

	/**
     * {@inheritDoc}
     */
	@Override
	public void reset() {
		state = State.WAITING_PSEUDO1;
        pseudoReader.reset();
        pseudo1 = null;
        pseudo2 = null;
	}
}
