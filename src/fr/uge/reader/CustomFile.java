package fr.uge.reader;

import java.nio.ByteBuffer;

/**
 * A class to store the arguments of the file request
 */
public class CustomFile {
	private long ID;
	private String filename;
	private String pseudo;
	private int part;
	private int total;
	private ByteBuffer data;
	
	/**
	 * instantiate the customFile
	 * @param ID the ID of the request
	 * @param filename the name of the file
	 * @param pseudo the pseudo which sends the requests
	 * @param part the actual part of the file
	 * @param total the total part of the file
	 * @param data the data
	 */
	public CustomFile(long ID, String filename, String pseudo, int part, int total, ByteBuffer data) {
		this.filename = filename;
		this.pseudo = pseudo;
		this.part = part;
		this.total = total;
		this.data = data;
		this.ID = ID;
	}

	/**
	 * getter for the file name
	 * @return the file name
	 */
	public String getFilename() {
		return filename;
	}

	/**
	 * getter for the pseudo
	 * @return the pseudo
	 */
	public String getPseudo() {
		return pseudo;
	}
	
	/**
	 * getter for the ID
	 * @return the ID
	 */
	public long getID() {
		return ID;
	}

	/**
	 * getter for the part
	 * @return the part
	 */
	public int getPart() {
		return part;
	}

	/**
	 * getter for the total
	 * @return the total
	 */
	public int getTotal() {
		return total;
	}
	
	/**
	 * getter for the datas
	 * @return the datas
	 */
	public ByteBuffer getData() {
		return data;
	}

	/**
	 * display the CustomFile object
	 */
	@Override
	public String toString() {
		return "CustomFile [filename=" + filename + ", pseudo=" + pseudo + ", part=" + part + ", total=" + total + "]";
	}
	
	
	/**
	 * convert an array of CustomFile to an array of ByteBuffer
	 * @param files all the CustomFile Objects
	 * @return an array of ByteBuffer
	 */
	public static ByteBuffer[] toByteBuffer(CustomFile[] files) {
		ByteBuffer[] bb = new ByteBuffer[files.length];
		for (int i = 0; i < files.length; i++) {
			bb[i] = files[i].getData().flip();
		}
		return bb;
	}
}
