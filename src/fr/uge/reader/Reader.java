package fr.uge.reader;

import java.nio.ByteBuffer;

/**
 * Interface used to generalize the process of reading frame in a ByteBuffer
 * @param <T> the Class to return when calling the methode get 
 */
public interface Reader<T> {

    /**
     * indicates the status of the decoding process, three differents states :
     * <ul>
     * 	<li> Done : The decoding process is finished, needs to call the reset method </li>
     * 	<li> Refill : The decoding process needs more data </li>
     * 	<li> Error : there were some issues while decoding </li>
     * </ul>
     */
    public static enum ProcessStatus {DONE,REFILL,ERROR};

    
    /**
     * method used to launch the decoding process
     * @param bb the buffer to analyze
     * @return the status of the decoding process
     */
    public ProcessStatus process(ByteBuffer bb);

    
    /**
     * get an instance of the Frame associated containing the arguments read during the decoding process
     * @return the values read
     */
    public T get();

    /**
     * reset the reader so that it cans uses the process method again
     */
    public void reset();
}
